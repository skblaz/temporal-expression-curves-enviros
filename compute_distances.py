## compute pairwise distances between the time series.
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
from scipy.spatial.distance import cosine

def compute_distance_distribution(dataframe, threshold = 0.7):
    
    relevant_columns = [
        'C18_11d_WS : C18_11d_WW', 'C18_34d_WS : C18_34d_WW',
        'C19_22d_WS : C19_22d_WW',  'C19_44d_WS : C19_44d_WW', 
        'C18_67d_WS : C18_67d_WW',  'F18_10d_WS : F18_10d_WW', 
        'F18_34d_WS : F18_34d_WW',  'F19_22d_WS : F19_22d_WW', 
        'F19_44d_WS : F19_44d_WW',  'F18_67d_WS : F18_67d_WW', 'geneID','BINCODE','BINNAME','DESCRIPTION']
    dataframe = dataframe[relevant_columns]
    dataframe_descriptions = dataframe[['geneID','BINCODE','BINNAME','DESCRIPTION']]    
    dataframe_cabernet = dataframe[['C18_11d_WS : C18_11d_WW', 'C18_34d_WS : C18_34d_WW', 'C19_22d_WS : C19_22d_WW',  'C19_44d_WS : C19_44d_WW', 'C18_67d_WS : C18_67d_WW']]    
    dataframe_fleurtai = dataframe[['F18_10d_WS : F18_10d_WW', 'F18_34d_WS : F18_34d_WW',  'F19_22d_WS : F19_22d_WW', 'F19_44d_WS : F19_44d_WW',  'F18_67d_WS : F18_67d_WW']]
    cabernet_matrix = dataframe_cabernet.values
    fleurtai_matrix = dataframe_fleurtai.values    
    assert cabernet_matrix.shape[1] == fleurtai_matrix.shape[1]
    gene_sim_results = []
    for k in range(cabernet_matrix.shape[0]):
        if not np.any(np.abs(cabernet_matrix[k]) >= threshold) or not np.any(np.abs(fleurtai_matrix[k]) >= threshold):
            continue
            
        similarity = 1-cosine(cabernet_matrix[k], fleurtai_matrix[k])
        gene = dataframe_descriptions.iloc[k].values.tolist() + [similarity]
        gene_sim_results.append(gene)
    fdf = pd.DataFrame(gene_sim_results)
    print(f"Found {fdf.shape} genes.")
    fdf.columns = ['geneID','BINCODE','BINNAME','DESCRIPTION','Similarity']
    sns.distplot(fdf.Similarity, color = "black")
    plt.xlabel("Cosine similarity")
    plt.ylabel("Density (amount of gene pairs)")
    plt.xlim(-1,1)
    plt.savefig(f"./results/similarity_density_{threshold}.png", dpi = 300)
    plt.clf()
    fdf.to_csv(f"./results/similarity_{threshold}.tsv", sep = "\t", index = False)
    
if __name__ == "__main__":
    joint_frame = pd.read_csv("./data/joint.csv",sep = "\t")
    thresholds = [0.3,0.5,0.7,1,2,3,4]
    for thresholdx in thresholds:
        compute_distance_distribution(joint_frame, threshold = thresholdx)
